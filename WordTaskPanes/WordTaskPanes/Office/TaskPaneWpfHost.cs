﻿using System.Drawing;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;

namespace WordTaskPanes.Office
{
    public sealed class TaskPaneWpfHost : UserControl
    {
        private readonly ElementHost _elementHost;

        public TaskPaneWpfHost()
        {
            _elementHost = new ElementHost();

            Controls.Add(_elementHost);

            Font = new Font("Calibri", 12F);
            BackColor = Color.White;
            AutoScaleMode = AutoScaleMode.Dpi;
        }

        public UIElement Child
        {
            get => _elementHost.Child;
            set
            {
                if (!(value is FrameworkElement))
                    return;

                _elementHost.Child = value;
                _elementHost.Width++;
                _elementHost.Dock = DockStyle.Fill;
            }
        }
    }
}