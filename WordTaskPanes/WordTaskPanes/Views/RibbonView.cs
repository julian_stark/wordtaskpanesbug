﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Office.Core;
using WordTaskPanes.Office;
using WordTaskPanes.Properties;
using CustomTaskPane = Microsoft.Office.Tools.CustomTaskPane;

namespace WordTaskPanes.Views
{
    [ComVisible(true)]
    public class RibbonView : IRibbonExtensibility
    {
        #region Fields

        private IRibbonUI _ribbon;
        private Dictionary<int, CustomTaskPane> _customTaskPanes = new Dictionary<int, CustomTaskPane>();

        #endregion

        #region Ribbon Callbacks

        public void Ribbon_Load(IRibbonUI ribbonUI)
        {
            _ribbon = ribbonUI;

            Globals.ThisAddIn.Shutdown += (sender, args) => { Cleanup(); };
        }

        public void OnOpen(IRibbonControl control)
        {
            var id = Globals.ThisAddIn.Application.ActiveDocument.DocID;

            if (!_customTaskPanes.ContainsKey(id))
            {
                CreateCustomTaskPane();
            }

            var customTaskPane = _customTaskPanes[id];
            if (customTaskPane == null)
                return;

            customTaskPane.Visible = !customTaskPane.Visible;
        }

        public void OnNewDocument(IRibbonControl control)
        {
            // TODO clear temp folder afterwards C:/Temp/CustomTaskPanes/

            var directory = Path.Combine(Path.GetTempPath(), "CustomTaskPanes");
            var fileName = Guid.NewGuid().ToString("N") + ".docx";

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            var filePath = Path.Combine(directory, fileName);

            File.WriteAllBytes(filePath, Resources.test);

            if (!File.Exists(filePath))
                return;

            Globals.ThisAddIn.Application.Documents.Open(filePath);
        }

        #endregion
        
        #region Create TaskPane

        private void CreateCustomTaskPane()
        {
            var userControl = new TaskPaneWpfHost { Child = new TaskPaneView() };
            var window = Globals.ThisAddIn.Application.ActiveWindow;
            
            var customTaskPane = Globals.ThisAddIn.CustomTaskPanes.Add(userControl, "My CustomTaskPane", window);
            if (customTaskPane == null)
            {
                MessageBox.Show(@"TaskPane could not be loaded", @"Error");
                return;
            }

            customTaskPane.DockPositionRestrict = MsoCTPDockPositionRestrict.msoCTPDockPositionRestrictNoHorizontal;
            customTaskPane.DockPosition = MsoCTPDockPosition.msoCTPDockPositionLeft;
            customTaskPane.Width = 400;

            var id = Globals.ThisAddIn.Application.ActiveDocument.DocID;
            _customTaskPanes.Add(id, customTaskPane);
        }

        #endregion

        #region Cleanup TaskPanes

        public void Cleanup()
        {
            foreach (var entry in _customTaskPanes)
            {
                try
                {
                    var customTaskPane = entry.Value;
                    if (customTaskPane == null)
                        return;

                    if (customTaskPane.Visible)
                        customTaskPane.Visible = false;

                    Globals.ThisAddIn.CustomTaskPanes.Remove(customTaskPane);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        #endregion

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("WordTaskPanes.Views.RibbonView.xml");
        }

        #endregion

        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
